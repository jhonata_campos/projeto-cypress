
describe("Tickets", () => {
    beforeEach(() => cy.visit("https://bit.ly/2XSuwCW"));

    it("fills all the text input", () => {//preencher todos os campos

        const firstName = "Jhonata";//declarou variavel firstName como Jhonata
        const lastName = "Campos";//declarou variavel firstName como Campos

        cy.get("#first-name").type(firstName);//preenchendo os campos
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("teste@vegetal.com");
        cy.get("#requests").type("Tem que ser especial");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    it("select two tickets", () => {
        cy.get("#ticket-quantity").select("2");//selecionando a opção 2 da lista
    });

    it("select 'vip' ticket type", () => {
        cy.get("#vip").check();//seleiona o radio button 'Vip'
    });

    it("selects 'social media' checkbox", () =>{
        cy.get("#social-media").check();//seleciona o checkbox
    });

    it("selects 'friend' and 'publication', then uncheck 'friend'", () =>{
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();//tira a seleção do checkbox
    });


    it("has 'CAIXA DE BILHETES' header´s heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");//compara o que tá no h1 do header se é igual a palavra
    });

    it("alerts on invalid email", () => {
        cy.get("#email")
        .as("emelho")
        .type("joaosantos-gmail.com");// o "as" deu um apelido a email

        cy.get("#email.invalid").should("exist");//compara se o alerta existe

        cy.get("@emelho")
          .clear()
          .type("joaocelestino@gmail.com");//limpou e colocou outro email

        cy.get("#email.invalid").should("not.exist");//vê se não existe alerta
    });

    it("fills and reset the form", () =>{
        const firstName = "Jhonata";//declarou variavel firstName como Jhonata
        const lastName = "Campos";//declarou variavel firstName como Campos
        const fullName = `${firstName} ${lastName}`;


        cy.get("#first-name").type(firstName);//preenchendo os campos
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("teste@vegetal.com");   
        cy.get("#ticket-quantity").select("2"); 
        cy.get("#vip").check();    
        cy.get("#friend").check();
        
        

        cy.get(".agreement p").should("contain", `I, ${fullName}, wish to buy 2 VIP tickets.`);

        cy.get("#agree").check();

        cy.get("#signature").type(`${fullName}`);

        cy.get("button[type='submit']")
          .as("submitButton") 
          .should("not.be.disabled");

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");

    });

    it.only("fills mandatory fields using support command", () =>{
        const customer = {
            firstName: "João",
            lastName: "Santos",
            email: "joaosantos@gmail.com"
        };

        cy.fillMandatoryFields(customer);//função criada recebendo "customer"
        
        cy.get("button[type='submit']")
          .as("submitButton") 
          .should("not.be.disabled"); 

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");
    });
});